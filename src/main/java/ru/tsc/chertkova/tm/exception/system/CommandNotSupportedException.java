package ru.tsc.chertkova.tm.exception.system;

import ru.tsc.chertkova.tm.exception.AbstractException;

public final class CommandNotSupportedException extends AbstractException {

    public CommandNotSupportedException() {
        super("Error! Command not supported...");
    }

    public CommandNotSupportedException(final String command) {
        super("Error! Command ``" + command + "`` not supported...");
    }

}
