package ru.tsc.chertkova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.api.repository.IRepository;
import ru.tsc.chertkova.tm.enumerated.Sort;
import ru.tsc.chertkova.tm.model.AbstractModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    protected final List<M> models = new ArrayList<>();

    @Nullable
    @Override
    public M add(@Nullable final M model) {
        models.add(model);
        return model;
    }

    @Override
    public void clear() {
        models.clear();
    }

    @Nullable
    @Override
    public List<M> findAll() {
        return models;
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        return findById(id) != null;
    }

    @Nullable
    @Override
    public M findById(@Nullable final String id) {
        return models.stream()
                .filter(item -> id.equals(item.getId()))
                .findFirst().get();
    }

    @Override
    public int getSize() {
        return models.size();
    }

    @Nullable
    @Override
    public M remove(@Nullable final M model) {
        models.remove(model);
        return model;
    }

    @Nullable
    @Override
    public M removeById(@Nullable final String id) {
        final M model = findById(id);
        if (model == null) return null;
        models.remove(model);
        return model;
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final Comparator comparator) {
        @NotNull final List<M> result = new ArrayList<>(models);
        result.sort(comparator);
        return result;
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final Sort sort) {
        @NotNull final List<M> result = new ArrayList<>(models);
        result.sort(sort.getComparator());
        return result;
    }

    @Override
    public void removeAll(@Nullable final Collection<M> collection) {
        models.removeAll(collection);
    }

}
